package com.example.air.hypeproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * @author Eren ATAS
 * DO NOT FORGET TO ADD YOUR TRACKING ID FROM app/src/res/xml/global_tracker.xml
 */
public class MainActivity extends AppCompatActivity {

    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sAnalytics = GoogleAnalytics.getInstance(this);
        sTracker = getDefaultTracker();
        sTracker.setScreenName("Main Screen"); //Sets the screen name to Main Screen.
        sTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    /**
     * @brief This method is for navigation to Sad page activity.
     * @param view
     */
    public void sadPageOnClick(View view){
        Intent i = new Intent(getApplicationContext(),SadActivity.class);
        startActivity(i);
    }

    /**
     * @brief This method gets the Google Analytics Tracker with the tracking number inside global_tracker.xml
     * @return
     */
    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }
        return sTracker;
    }

    /**
     * @brief This method is for navigation to heart page activity.
     * @param view
     */
    public void heartPageOnClick(View view) {
        Intent i = new Intent(getApplicationContext(),HeartActivity.class);
        startActivity(i);
    }
}
